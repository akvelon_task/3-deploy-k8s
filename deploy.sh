#! /bin/bash

#configure AWSCLI

export dnsname=alusk8s.k8s.local
export clustername=akvelon-cluster
export bucketname=akvelon-bucket
export KOPS_STATE_STORE=s3://${bucketname}


aws s3api create-bucket --bucket ${bucketname}
#aws s3api put-bucket-versioning --bucket ${bucketname} --versioning-configuration Status=Enabled

kops create cluster \
--name ${clustername}.${dnsname} \
--zones eu-west-1a \
--master-count 1 \
--node-count 2 \
--master-size t2.micro \
--node-size t2.micro \
--state ${KOPS_STATE_STORE} \
--dry-run --output yaml | tee ${clustername}.${dnsname}.yaml

sed -i 's/.*maxSize.*/  maxPrice: "0.20"\n&/' ${clustername}.${dnsname}.yaml

cat ${clustername}.${dnsname}.yaml

kops create -f ${clustername}.${dnsname}.yaml

kops create secret --name ${clustername}.${dnsname} sshpublickey admin -i ~/.ssh/id_rsa.pub

kops update cluster --name ${clustername}.${dnsname} --yes


sleep 600
kops validate cluster
kubectl get nodes --show-labels
# kops get instancegroups --name ${clustername}.${dnsname} 
# kops edit instancegroups master-eu-west-1a --name ${clustername}.${dnsname} 
# kops edit instancegroups nodes --name ${clustername}.${dnsname} 


#https://github.com/kubernetes/kops/blob/master/docs/addons.md

kubectl create -f https://raw.githubusercontent.com/kubernetes/kops/master/addons/kubernetes-dashboard/v1.8.3.yaml

echo "apiVersion: rbac.authorization.k8s.io/v1beta1 
kind: ClusterRoleBinding 
metadata: 
  name: kubernetes-dashboard 
  labels: 
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system" > full.yml

kubectl create -f full.yml

#kops get secrets kube --type secret -oplaintext
#kubectl config view --minify

#https://kubernetes.io/docs/reference/access-authn-authz/rbac/#permissive-rbac-permissions

kubectl create clusterrolebinding permissive-binding \
  --clusterrole=cluster-admin \
  --user=admin \
  --user=kubelet \
  --group=system:serviceaccounts


# https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/index.md#aws


kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/aws/service-l4.yaml


#cp /home/berni/.kube/config /home/berni/snap/helm/common/kube/
#helm init
#helm install stable/drupal


# https://github.com/kubernetes/examples/tree/master/guestbook-go

kubectl create -f examples/guestbook-go/redis-master-controller.json
kubectl create -f examples/guestbook-go/redis-master-service.json
kubectl create -f examples/guestbook-go/redis-slave-controller.json
kubectl create -f examples/guestbook-go/redis-slave-service.json
kubectl create -f examples/guestbook-go/guestbook-controller.json
kubectl create -f examples/guestbook-go/guestbook-service.json



# https://github.com/nginxinc/kubernetes-ingress/tree/master/helm-chart
# cd kubernetes-ingress/helm-chart
# helm install --name ingressnginx-release .

# https://github.com/nginxinc/kubernetes-ingress/tree/master/examples/complete-example





### kops delete cluster --name ${clustername}.${dnsname} --yes

